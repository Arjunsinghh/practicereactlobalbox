import React, { Component } from 'react'
import LoadingAnimation from '../../components/loadingAnimation'
import './stylesheets/landingMarketsegment.css'
import jumpTo,{go} from '../../modules/Navigation'


export default class landingMarketsegment extends Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
      this.props.getProductCountSagment()
  }
  render() {
    const logocompany = require('../../assets/images/GBX-silver-logo.png');
    const logobusiness = require('../../assets/images/business.png');
    const logoconsumer = require('../../assets/images/consumer.png');
    const logobussgover = require('../../assets/images/buss_gover.png');
    const logomanubusiness = require('../../assets/images/manu_business.png');
    const logomanuconsum = require('../../assets/images/manu_consum.png');
    const logomanugover = require('../../assets/images/manu_gover.png');
    const logogovernment = require('../../assets/images/government.png');
    const logogovbusiness = require('../../assets/images/gov_business.png');
    const logoconsumerto = require('../../assets/images/consumer-to.png');
    const productCountBTB = 20;
    const productCountBTC = 20;
    const productCountBTG = 20;
    const productCountMTB = 20;
    const productCountMTC = 20;
    const productCountMTG = 20;
    const productCountGTG = 20;
    const productCountGTB = 20;
    const productCountCTC = 20;
    return (
      <span>
          <a href="/home" className="GBX_logo">
            <img src={logocompany}/>
          </a>

          <div className="Filter_heading container">
            <div className="sub_back text-left">
              <a onClick={() => jumpTo('landing-marketplace')} className="land_back"><i className="fas fa-arrow-left"></i><span> Back</span></a>
              <h2 className="shop_head">Filter by Market Segments</h2>
            </div>
          </div>

          <div className="shopby blue_bg_main">
            <div className="container">
              <div className="market_seg">
                <div className="row margin_7">
                  <div className="col-sm-4">
                    <a className="recommend_box busines_bus segment_box">
                        <div className="title_box">B2B</div>
                        <div className="recommend-icon segmnt-icon">
                          <img src={logobusiness} />
                        </div>
                        {this.props.productCount !== null ? this.props.productCount.marketSagment.B2B :''}
                        <span className="recommend-border"></span>
                        <p className="recommend-below-title segmnt_blw">Business 2 Business</p> 
                    </a>
                  </div>
                  <div className="col-sm-4">
                    <a className="recommend_box busines_con segment_box">
                    
                      <div className="recommend_txt">
                        <div className="title_box">B2C</div>
                        <div className="recommend-icon segmnt-icon">
                          <img src={logoconsumer} />
                        </div>
                        {this.props.productCount !== null ? this.props.productCount.marketSagment.B2C :''}
                        <span className="recommend-border"></span>
                        <p className="recommend-below-title segmnt_blw">Business 2 Consumer</p>
                      </div>  
                    </a>
                  </div>
                  <div className="col-sm-4">
                    <a className="recommend_box bus_gov1 segment_box">
                      <div className="recommend_txt">
                        <div className="title_box">B2G</div>
                        <div className="recommend-icon segmnt-icon">
                          <img src={logobussgover} />
                        </div>
                        {this.props.productCount !== null ? this.props.productCount.marketSagment.B2G :''}
                        <span className="recommend-border"></span>
                        <p className="recommend-below-title segmnt_blw">Business 2 Government</p>
                      </div>  
                    </a>
                  </div>
                  <div className="col-sm-4">
                    <a className="recommend_box man_bus segment_box">
                      <div className="recommend_txt">
                        <div className="title_box">M2B</div>
                        <div className="recommend-icon segmnt-icon">
                          <img src={logomanubusiness} />
                        </div>
                        {this.props.productCount !== null ? this.props.productCount.marketSagment.M2B :''}
                        <span className="recommend-border"></span>
                        <p className="recommend-below-title segmnt_blw">Manufacturer 2 Business</p>
                      </div>  
                    </a>
                  </div>
                  <div className="col-sm-4">
                    <a className="recommend_box man_con segment_box">
                      <div className="recommend_txt">
                        <div className="title_box">M2C</div>
                        <div className="recommend-icon segmnt-icon">
                          <img src={logomanuconsum} />
                        </div>
                        {this.props.productCount !== null ? this.props.productCount.marketSagment.M2C :''}
                        <span className="recommend-border"></span>
                        <p className="recommend-below-title segmnt_blw">Manufacturer 2 Consumer</p>
                      </div>  
                    </a>
                  </div>
                  <div className="col-sm-4">
                    <a className="recommend_box man_gov segment_box">
                      <div className="recommend_txt">
                        <div className="title_box">M2G</div>
                        <div className="recommend-icon segmnt-icon">
                          <img src={logomanugover} />
                        </div>
                        {this.props.productCount !== null ? this.props.productCount.marketSagment.M2G :''}
                        <span className="recommend-border"></span>
                        <p className="recommend-below-title segmnt_blw">Manufacturer 2 Government</p>
                      </div>  
                    </a>
                  </div>
                  <div className="col-sm-4">
                    <a className="recommend_box gov_gov segment_box">
                      <div className="recommend_txt">
                        <div className="title_box">G2G</div>
                        <div className="recommend-icon segmnt-icon">
                          <img src={logogovernment} />
                        </div>
                        {this.props.productCount !== null ? this.props.productCount.marketSagment.G2G :''}
                        <span className="recommend-border"></span>
                        <p className="recommend-below-title segmnt_blw">Government 2 Government</p>
                      </div>  
                    </a>
                  </div>  
                  <div className="col-sm-4">
                    <a className="recommend_box gov_bus segment_box">
                      <div className="recommend_txt">
                        <div className="title_box">G2B</div>
                        <div className="recommend-icon segmnt-icon">
                          <img src={logogovbusiness} />
                        </div>
                        {this.props.productCount !== null ? this.props.productCount.marketSagment.G2B :''}
                        <span className="recommend-border"></span>
                        <p className="recommend-below-title segmnt_blw">Government 2 Business</p>
                      </div>  
                    </a>
                  </div>
                  <div className="col-sm-4">
                    <a className="recommend_box con_con segment_box">
                      
                      <div className="recommend_txt">
                        <div className="title_box">C2C</div>
                        <div className="recommend-icon segmnt-icon">
                          <img src={logoconsumerto} />
                        </div>
                        {this.props.productCount !== null ? this.props.productCount.marketSagment.C2C :''}
                        <span className="recommend-border"></span>
                        <p className="recommend-below-title segmnt_blw">Consumer 2 Consumer</p>
                      </div>  
                    </a>
                  </div>
              </div>  
            </div>
          </div>
        </div>
      </span>
    )
  }
}





