import React, { Component } from 'react'
import LoadingAnimation from '../../components/loadingAnimation'
import './stylesheets/landingProvince.css'
import jumpTo,{go} from '../../modules/Navigation'


export default class landingProvince extends Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    this.props.getProductCountProvince();
  }
  render() {
    const logocompany = require('../../assets/images/GBX-silver-logo.png');
    
    return (
      <span>
          <a href="/home" className="GBX_logo">
            <img src={logocompany}/>
          </a>

          <div className="shopIn_heading container">
            <div className="sub_back text-left">
              <a onClick={() => jumpTo('landing-marketplace')} className="land_back"><i className="fas fa-arrow-left"></i><span> Back</span></a>
              <h2 className="shop_head">Shop In: {this.props.productCount !== null ? this.props.productCount.state+", "+this.props.productCount.countryName :''}</h2>
            </div>
          </div>
          <div className="shopby">
            <div className="container">
                <div className="row margin_7">
                  <div className="col-sm-6">
                    <a className="recommend_box locate_jaland">          
                      <div className="recommend_txt">
                        <div className="recommend-icon"><i className="fas fa-map-marked-alt"></i></div>
                        {this.props.productCount !== null ? this.props.productCount.ProvinceStateProductCount :''}
                        <span className="recommend-border"></span>
                        <p className="recommend-below-title">Products<br/> LOCATED in {this.props.productCount !== null ? this.props.productCount.state :''}</p>
                      </div>  
                    </a>
                  </div>
                  
                 
                  <div className="col-sm-6">
                    <a className="recommend_box vendors_lo">
                    <div className="hexa_logo">
                      </div>
                    <div className="recommend_txt">
                      <div className="recommend-icon"><i className="fas fa-user-tie"></i></div>
                      {this.props.productCount !== null ? this.props.productCount.ProvinceStateVendorCount :''}
                      <span className="recommend-border"></span>
                      <p className="recommend-below-title">Vendors <br/>LOCATED in {this.props.productCount !== null ? this.props.productCount.state :''}</p>
                    </div>  
                    </a>
                  </div>
                </div>
            </div>
          </div>
      </span>
    )
  }
}





