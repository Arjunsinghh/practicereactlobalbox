import React, { Component } from 'react'
import FooterContainer from '../../components/footer/footerContainer'
import './stylesheets/privacyPolicy.css'
import jumpTo,{go} from '../../modules/Navigation'


export default class privacyPolicy extends Component {

  render() {
    
    return (
<div className="pro_midcontianer mid_container">
  <div className="container">
    <div className="col-md-12 terms">
      <h2 className="dark-grey"><b>Summary of Privacy Policy under the EU-U.S. Privacy Shield</b></h2>
      <br/>
      <p>
        This summary highlights the key points of our Privacy Statement.
      </p>
      <p>Please note that in the context of this disclosure; we, us, our, means primarily the GlobalBox.world platform</p>
      <p>We care about your privacy and have a dedicated data privacy program. We do not and will not sell or rent your data unless this is required in the context of a change to our business structure. 
      </p>
      <p><i className="fa fa-circle fa-1" aria-hidden="true"></i> This Statement applies to our GlobalBox.world websites</p>
      <p><i className="fa fa-circle fa-1" aria-hidden="true"></i> GlobalBox.world DOES NOT share your personal information with any third party under any circumstances, unless such third parties are part of the GlobalBox.world community; such as Vendors, Affiliates, Logistics Providers, Advertiser, or our own staff.</p>
      <p><i className="fa fa-circle fa-1" aria-hidden="true"></i> We do, however, conduct some marketing campaigns in order to promote our products and services. This marketing is aimed at our current and potential clients and partners. </p>
      <p><i className="fa fa-circle fa-1" aria-hidden="true"></i> We do, however, conduct some marketing campaigns in order to promote our products and services. This marketing is aimed at our current and potential clients and partners. </p>
      <p><i className="fa fa-circle fa-1" aria-hidden="true"></i> We do not collect any personal information from any person under the age of 18.</p>
      <p><i className="fa fa-circle fa-1" aria-hidden="true"></i> We employ a variety of physical, technological and administrative security safeguards designed to protect everyone’s personal information.</p>
      <p><i className="fa fa-circle fa-1" aria-hidden="true"></i> All the sensitive data, passwords, and other sensitive information pertaining to our clients is encrypted in order to prevent its access through fraud.</p>
      <p><i className="fa fa-circle fa-1" aria-hidden="true"></i> Please note that due to the worldwide nature of our platform, we may transfer your information to locations outside of your country.</p>
      <p><i className="fa fa-circle fa-1" aria-hidden="true"></i> You can contact us at <a>notifications@GlobalBox.world</a>  if you want to exercise your privacy rights, have a complaint, or want to ask a question. </p>
      <p><i className="fa fa-circle fa-1" aria-hidden="true"></i> This Statement was last updated on May 24, 2018</p>
      <br/>
      <h3><b> Introduction</b></h3>
      <p><b>We care about privacy.</b> We believe that privacy is a fundamental right for all individuals. We take the obligations that are attached to this information very seriously. We therefore have a dedicated data privacy program with privacy by design at heart.</p>
      <p>Our business model is different from that of companies that collect your personal information with an intent to monetize such data. We collect and use personal information ONLY in order to allow us to provide our customers and end users with the relevant products and services, especially as they pertain to the actual location of the user. <b> We do not and will not sell or rent your data to third parties </b>unless this might be required in the context of changes to our business structure such as a merger or acquisition.</p>
      <p>We are working to become EU-U.S. Privacy Shield certified, and thus a signatory of the Privacy Pledge, and a member of the Future of Privacy Forum.</p>
      <br/>
      <br/>
      <br/>
      <br/>
      <p><b>Who we are. </b>When we refer to “us,” “we,” “our,” or “GlobalBox.world” in this Statement, we mean GlobalBox.world Inc. and its affiliates.</p>
      <p><b>This Statement governs all our services that we provide directly to you.</b> Whether you are browsing our websites, receive our newsletters, or any marketing material from us, this Statement governs the use of personal information for all our products and services that we provide directly to you as a so-called ‘data controller.’</p>
      <p><b>When your institution/company privacy statement/policy governs.</b> If you are an end-user of our client and we are providing our products and services to you on our client’s (your institution/company) behalf, we are considered a ‘data processor.’ In this case your institution’s privacy statement governs the use of personal information. Our Privacy Statement does not supersede the terms of any agreements between us and your institution/company (or any other client or third party), nor does it affect the terms of any agreement between you and your institution/company.</p>
      <p><b>Changes to this Statement.</b> From time to time we will need to update this Statement to reflect changes to our products and services, the way we operate, or to meet new legal and regulatory requirements.</p>
      <br/>
      <br/>
      <br/>

      <h3><b>How We Use Your Information</b></h3>
      <br/>
      <p>Any personal information we collect will be used SOLELY for the purpose of better personalizing our product and services offerings to you. Once again, please note that WE WILL NOT SELL, RENT, OR OTHERWISE SHARE YOUR INFORMATION WITH ANY THIRD PARTY, unless it is done for legal purposes, or in a potential case of a merger or acquisition that may take place in the future.</p>
      <br/>
      <h3><b>Website Users</b></h3>
      <br/>
      <p>GlobalBox.world is a worldwide platform, and therefore the site operates in all geographic market segments, which are accessible to anyone who has access to the internet. You generally can visit our website without having to log in or otherwise identify yourself, although in order for our platform to properly showcase all its features, we do need to collect information about your physical location. In order to access functionalities relating to services offered by our platform; however, you will need to log in. This will give you the full benefit of our services and will allow you to make use of all the functionalities offered by the site, as well as for your to be able to communicate with us, or other GlobalBox.world users, such as with GlobalBox.world Community and other user of the platform.</p>
      <br/>
      <h3><b>Information We Collect</b></h3>
      <p><i className="fa fa-circle fa-1" aria-hidden="true"></i>  <b>Directly from you.</b> In order for you to take advantage of all our offered functionalities, our website requires you to register and thus to sign up for an account. When you do, we ask you for information, such as your name and email address, your physical address, and your age. This information allows us to determine your eligibility for participating on our platform, as NO person younger than 18 years of age is allowed to participate on t his platform. This information is required in order to set up your account. Often you can also voluntarily provide additional information, such as a photo or biographical information, to make better use of our websites. We might also collect information you provide by completing forms or surveys on our websites. If you provide additional content through user forums, for example, when you communicate with others on GlobalBox.world Community, we may link this information with your personal information if you are logged in.</p>
      <p><i className="fa fa-circle fa-1" aria-hidden="true"></i>  <b>Indirectly from you.</b> We collect information about the pages and the products you visit and how you access and use our websites using cookies and analytics tools. This information includes information about the device(s) you use to access the websites including unique device identifiers, IP address, operating system, browser, and cookies. Depending on your device settings, we will also collect information about your geographical location. </p>
      <p><i className="fa fa-circle fa-1" aria-hidden="true"></i>  <b>Information from third parties.</b> We receive information from affiliates in our GlobalBox.world group of companies, our partners, and other third parties that we use to enhance the website experience and promote our products and services. For example, we will receive information from some third parties about how well an online marketing or email campaign performed.</p>
      
      <br/>
      <h3><b>How We Use This Information</b></h3>
      <p>We use this information to provide and improve our website services as follows.</p>
      <p><b>Analytics and marketing.</b> We analyze usage information for sales and marketing purposes and for trends about our visitors and their demographics and how they use our website. This analysis is necessary to further our legitimate interest in understanding our users and how they interact with us and our website, improving our website and communication with you (including for marketing purposes).</p>
      <p><b>Personalization.</b> We will also use your information in order to personalize the products, which is necessary in our effort to provide more contextually relevant information as you browse our website.</p>
      <p><b>Communication.</b> If you use the GlobalBox.world site, GlobalBox.world Community, we use the contact details you provided to communicate with you and to let you communicate with us and other GlobalBox.world users. Typically, we use information in this way because it is required to provide this service to you and because it is in our legitimate interests to ensure our services are being offered in a manner that meets our and your high standards.</p>
      <p>We keep website usage information for as long as it is necessary for the purposes described above. This period of time will vary depending on the services and websites you use and how you interact with us. Account information is kept until you delete the account and for a limited period of 60 months afterwards. For example, we may need to keep a record of the correspondence with you if you have made a complaint for as long as it is necessary to protect us from any legal claim. Similarly, if you unsubscribe from receiving marketing communications from us, will keep a record of this to ensure you do not receive such communications in the future.</p>
      <br/>
      <h3><b>Who We Share This Information With</b></h3>
      <p><b> WE DO NOT SHARE YOUR INFORMATION WITH ANY THIRD PARTY – UNLESS THE THIRD PARTY IS A MEMBER OF THE GLOBALBOX.WORLD COMMUNITY (VENDORS, BUYERS, AFFILIATES, ADVERTISERS, LOGISTICS PROVIDERS, etc. AND/OR UNLESS IT IS DONE FOR LEGAL PUROPSES AS INSTRUCTED BY LEGAL AUTHORITIES, OR IN A FUTURE POTENTIAL CASE OF A MERGER OR ACQUISITION</b></p>
      <p><b>Partners.</b> WE DO; however, share personal information with the vendors, buyers, logistics providers and other partners, who may use such information for product deliveries, inquiries, and other possible marketing activities. </p>
      <p><b>Vendors.</b> We also share information with our vendors and service providers and other third parties for legal or business purposes. </p>
      <br/>
      <h3><b>Vendors, Partners & Other Types of Disclosures</b></h3>
      <p>This section provides more information on how we protect your information when we engage vendors, how we share information with our partners, and in which other scenarios we may share your information with third parties.</p>
      <br/>
      <h3><b>Vendors</b></h3>
      <p>We use vendors to help us provide products and services to our clients. Where this requires access to personal information, we are responsible for the data privacy practices of the vendors. Our vendors must abide by our strict data privacy and security requirements and instructions. They are not allowed to use personal information they access or receive from us for any other purposes than as needed to carry out their work for us.</p>
      <br/>
      <h3><b>Partners</b></h3>
      <p>In some countries and regions, our products and services are offered through channel (or reselling). We might share information with them that is necessary for them to offer and provide our products and services to our current and prospective clients.</p>
      <br/>
      <br/>
      <p>Some of our products allow you to access functionalities or content provided by our content and technology partners.</p>
      
      <br/>
      <h3><b>Other types of disclosures </b></h3>
      <p>We will also share your information where required in the following circumstances. </p>
      <p><i className="fa fa-circle fa-1" aria-hidden="true"></i>  <b>Payments.</b> Where you use our products and services, and subsequently make a purchase or financial transaction, we will share your payment and transaction data with banks and other organizations that process the transaction. This is done for your protection and for fraud detection and prevention or anti-money laundering purposes.</p>
      <p><i className="fa fa-circle fa-1" aria-hidden="true"></i>  <b>Changes to our business structure.</b> Where permitted by applicable law and by the contracts with our clients, we may disclose your information in the following cases:  </p>
      <p>Corporate transactions such as a merger, acquisition, sale of assets, and financing </p>
      <p>Bankruptcy, dissolution or reorganization, or in similar transactions or proceedings  </p>
      <p>Steps related to the previous bullet points (for example, due diligence)</p>
      <p><i className="fa fa-circle fa-1" aria-hidden="true"></i>  <b>Comply with law.</b> We may need to disclose your information to comply with legal or regulatory requirements and to respond to lawful requests, court orders, and legal processes. We will always aim to limit the information we provide as much as possible. Where such disclosures relate to personal information we hold on behalf of our clients, we will defer such requests to our clients where permissible.</p>
      <p><i className="fa fa-circle fa-1" aria-hidden="true"></i>  <b>Enforce our rights, prevent fraud, and for safety.</b> We may need to disclose your information to protect and defend the rights, property, or safety of us, our clients, or third parties, including enforcing contracts or policies or in connection with investigating and preventing fraud.</p>
      <p><i className="fa fa-circle fa-1" aria-hidden="true"></i>  <b>De-identified information.</b> We may disclose aggregate or de-identified information that is no longer associated with an identifiable individual for research or to enhance and promote our products and services. For example, we may share aggregated or de-identified information with our partners or others for business or research purposes like partnering with a research firm to explore how our products are being used and how such data can be used to enhance our functionalities and further help our clients. We will implement appropriate safeguards before sharing information, which may include removing or hashing direct identifiers (e.g., your name, email address, and device ID).</p>
      <br/>
      <h3><b>Client Engagement & Marketing</b></h3>
      <br/>
      <br/>
      <h3><b>Client engagement</b></h3>
      <br/>
      <br/>
      <p><b>Managing the client relationship.</b> We are collecting and storing limited personal information about the relevant contacts at our clients for invoicing, notification of product updates and maintenance, and similar purposes.</p>
      <br/>
      <h3><b>Marketing</b></h3>
      <p><b>Promotion of products and services.</b> We conduct marketing to promote our products and services. This marketing is generally aimed at our current and potential clients, customer, and partners. However, we do not restrict activities and events to those audiences when such activities and events benefit other end users of systems, such as info pages that explain how our products can be used effectively.</p>
      <p><b>Sharing within GlobalBox.world.</b> We may share personal information related to marketing with the relevant GlobalBox.world affiliates and departments. For example, information from a local Sales team may be provided to the Global Field Marketing and Marketing Operations teams, with our Affiliates, in order to update the relevant products and other promotional communications to you.</p>
      <p><b>Sharing with vendors.</b> We may use vendors to help us organize and conduct campaigns, events, and other aspects of marketing. We will share with them only the personal information that is necessary and ensure that they are following our strict requirements for vendors.</p>
      <p><b>Marketing preferences and opt-out.</b> Our marketing emails will include a link so that you can change your preferences and opt-out of receiving marketing communications from us. You can do this by clicking on the "Unsubscribe" link in the email footer which will direct you to our Marketing Preference Center. While it doesn’t give you the same detailed controls as our Marketing Preference Center, you can also send us email at <b>notifications@GlobalBox.world</b> to unsubscribe.</p>
      <br/>
      <h3><b>Online and interest-based advertising.</b></h3>
      <p>We may use third party advertising tools to collect information about your visits to our websites to serve you targeted advertisements based on your browsing history and interests on other websites and online services or on other devices you may use. In some instances, we may share a common account identifier (such as an email address or user ID) with our third-party advertising partners to help identify and contact you across devices. We and our third-party partners use this information to make the advertisements you see online more relevant to your interests, as well as to provide advertising-related services such as reporting, attribution, analytics and market research.</p>
      <p><b>Google Analytics and Advertising.</b> We may also utilize certain forms of display advertising and other advanced features through Google Analytics, such as Remarketing with Google Analytics, Google Display Network Impression Reporting, the DoubleClick Campaign Manager Integration, and Google Analytics Demographics and Interest Reporting. These features enable us to use first-party cookies (such as the Google Analytics cookie) and third-party cookies (such as the Doubleclick advertising cookie) or other third-party cookies together to inform, optimize, and display ads based on your past visits to our websites.</p>
      <br/>
      <h3><b>Security</b></h3>
      <p>We employ a variety of physical, administrative, and technological safeguards designed to protect personal information against loss, misuse, and unauthorized access or disclosure. We have dedicated information security programs and work hard to continuously enhance our technical and operational security measures.</p>
      <p>Our measures consider the sensitivity of the information we collect, use, and store, and the current state of technology. Our security measures include data encryption, firewalls, data use, and access limitations for our personnel and vendors and physical access controls to our facilities.</p>
      <p>All products and services that use payment data maintain the applicable Payment Card Industry (PCI) compliance levels. Our compliance with the PCI standards is validated during annual audits that are conducted by external auditors (so called ‘Qualified Security Assessors’).</p>
      <br/>
      <h3><b>Data Transfers & Additional Regional & Country Information</b></h3>
      <br/>
      <h3><b>Data Transfers</b></h3>
      <br/>
      <p>GlobalBox.world is a global company headquartered in Canada. Nevertheless, we do have a regional hosting strategy, which may require us to access your information from locations outside of your region and country, for support and maintenance purposes where permitted under applicable law and our contract with your institution. We understand the importance of complying with data transfer requirements. We use approved data transfer mechanisms, such as the EU-U.S. Privacy Shield (see below), to ensure the adequate protection of your information when it is transferred. </p>
      <br/>
      <h3><b>EU-U.S. Privacy Shield</b></h3>
      <p>We comply with the EU-U.S. Privacy Shield Framework regarding the collection, use, and retention of personal information from European Union member countries. Please learn more about GlobalBox.world’s compliance with the EU-U.S. Privacy Shield Principles here: <b>https://www.privacyshield.gov/</b> </p>
      <p>If you have any questions about our Privacy Shield Statement and related practices, please email us at notifications@GlobalBox.world</p>
      <br/>
      <h3><b>Your Rights</b></h3>
      <p>In the EU and many other jurisdictions, you have rights to control how your personal information is used. You may have the right to request access to, rectification of, or erasure of personal information we hold about you. In the EU, you also may have the right to object to or restrict certain types of use of your personal information and request to receive a machine-readable copy of the personal information you have provided to us.</p>
      <p>In many cases, you will be able to access your information as well as change and delete some of the information yourself by logging into your account. If you cannot access, correct, or delete the required information yourself, please contact us at: notifications@GlobalBox.world</p>
      <p>Please remember that many of these rights are not absolute. In some circumstances, we (or your institution) are not legally required to comply with your request because of relevant legal exemptions.</p>
      <p>In many jurisdictions, you also have the right to lodge a complaint with the local data protection authority. But please contact us first, so we can do our best in our effort to address your concern.</p>
      <br/>
      <h3><b>United States – Your California Privacy Rights</b></h3>
      <p>If you are a California resident, California Civil Code Section 1798.83 allows you to request information on how we disclose personal information to third parties for their direct marketing purposes during the immediately preceding calendar year. You may make one request each year by emailing us at: notifications@GlobalBox.world</p>
      <br/>
      <h3><b>Contact Us</b></h3>
      <p>If you have any questions or concerns about our Privacy Statement or data privacy practices, please contact us at: notifications@GlobalBox.world</p>
      
    </div>
  </div>
      {/* Footer */}
        <FooterContainer />
</div>
    )
  }
}





