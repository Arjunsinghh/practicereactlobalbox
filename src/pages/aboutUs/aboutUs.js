import React, { Component } from 'react'
import FooterContainer from '../../components/footer/footerContainer'
import './stylesheets/aboutUs.css'
import jumpTo,{go} from '../../modules/Navigation'


export default class aboutUs extends Component {

  render() {
    
    return (
 <div className="pro_midcontianer mid_container">
    <div className="container">
      <div className="col-md-12 terms">
          <h2 className="dark-grey"><b>GlobalBox.world - About us:</b></h2>
          <br/>
      
          <p>
          GlobalBox.world has been developed into a platform where just about any vendor or corporation in the world will be able to post and promote their products and services for sale. This platform offers 3 distinct components:
          </p>
          <br/>
      <p>-  Global Marketplace</p>
      <p>-  Global Services</p>
      <p>-  Global Promotions</p>
      <br/>
      <p>GlobalBox brings a unique and differentiated approach to the digital commerce space, by simply developing and implementing a platform which allows vendors and service providers from all over the world to post their products and services for Sale/Services/Promotion in their respective countries and also in their respective currencies, as well as at the same time allowing buyers to acquire any item from anywhere in the world in their respective currencies.</p>
      <br/>
      <p>
        In essence, the GlobalBox platform is intended to become a “Google”-type search engine for businesses, products, and services rendered worldwide by every corporation looking to sell or perform services either Business to Business, Business to Consumer, or even Consumer to Consumer. Moreover, GlobalBox will further facilitate a particular digital market channel which relates to Business to Government products and services offerings. This will be done at a true Global scale, and thus we anticipate that <h5><b>GlobalBox will become the very first organization in the world to facilitate trade and commerce across all national boundaries, with buyers and sellers having the ability to trade in every single country around the world.</b></h5>
      </p>
      <br/>
      <p>GlobalBox.world intends to redefine the internet-related commerce by shifting the traditional E-commerce (Electronic Commerce) space toward the F-commerce, which is defined as FACILITATIVE COMMERCE.</p>
      <br/>
      <p>In conclusion, it is anticipated that GlobalBox will integrate and aggregate the type of services rendered by companies such as Amazon, Alibaba, eBay, Groupon, Shopify, Craigslist, Etsy, etc. into one world-wide platform.</p>
      <br/>
      <h3><b>Services to be performed</b></h3>
      <p>The main service GlobalBox will offer is the listing, and subsequent searching and prioritization, of products and services offered by global vendors in one ‘uber’-convenient place. In actuality, the key benefit will be represented by the ease and convenience of accessing any* type of Product, Service, or Promotion that a customer may be looking for at any point in time and space on the face of the planet.</p>
      <br/>
      <p>The platform is primarily a facilitative one, without the company actually becoming a purchaser and subsequently a reseller of goods, products, and services.</p>
      <br/>
      <p>In the <b>Global Marketplace segment</b> the platform will simply facilitate the listing of all products and services from around the world in one convenient and easy to access place. Listed products will be then segmented, clustered, or sorted based on Keyword relevance, Price levels, Location, Category, Most Viewed, etc.</p>
      <br/>
      <h3><b>Main customers</b></h3>
      <p>Primary customer segments are represented on one hand by providers (vendors of goods, products, and services), and on the other hand by buyers who are interested in acquiring such products, services, or promotions. While most corporations are spending significant resources (time, money, effort) in identifying their market segments, and in subsequently further clustering those segments in order to be better able to serve the target customers, <b>GlobalBox will target just about every single corporation, company, vendor, promoter, developer, service provider, and consumer on the face of the planet.</b></p>
      <p>In fact, we are <b>looking to position GlobalBox as the foremost interactive platform for buyers and sellers everywhere in the world, and we will strive to ensure that within a decade every human being on the face of the planet will trade on this platform.</b> Maybe this is a very ambitious goal and objective, but in fact if someone was to say 20 years ago that a company named “Google” will become the best-known brand in the world within 20 years, most of the evaluators would have likely said that such a thing is quite likely impossible. Yet, today everyone knows who and what Google is, and the skeptics of 20 years past have now been proven utterly incorrect in their assumptions.</p>
      
      <br/>
      <h3><b>Company strategy and differentiation approaches</b></h3>
      <br/>
      <br/>
      <h4><b>Entrepreneur’s vision:</b></h4>
      <br/>
      <h4>Vision statement</h4>
      <br/>
      <p><b>“We aim to be the world’s leading facilitator of E-commerce and M-commerce transactions. Our interactive platform is designed to add value to the Global Community for generations to come”</b></p>
      <br/>
      <h4>Mission Statement</h4>
      <br/>
      <p><b>“Our mission is an ongoing P.R.O.C.E.S.S.
      </b>
        <b> “Protect, Respect, Organize, Connect, Evaluate, through Superior Strategy”</b></p>
      <br/>
      <h4>Long-term objectives</h4>
      <br/>
      <p>“Our long-term objective is to facilitate the convergence of all market forces (providers and consumers) on the face of the planet, <b>on the most convenient and straightforward internet-based platform”</b></p>
      <br/>
      <br/>
      <br/>
      <br/>
      <br/>
      <h5><b>*GlobalBox </b>will not permit or condone the commercialization of any illegal or inappropriate items</h5>
    
      </div>
    </div>
      {/* Footer */}
        <FooterContainer />
</div>
    )
  }
}





