import React, { Component } from 'react'
import FooterContainer from '../../components/footer/footerContainer'
import './stylesheets/contactUs.css'
import jumpTo,{go} from '../../modules/Navigation'


export default class contactUs extends Component {

  render() {
    
    return (
    
<div classname="pro_midcontianer mid_container">
  <div className="container">   
      <p>Contact Us</p>
  
    <div id='content'>
      <div id='left'>
      <ul id='location-bar'>
          <li>
            <a className='location' data-location='Canada' href='javascript:void(0)'>Canada</a>
          </li>
          <li>
            <a className='location' data-location='USA' href='javascript:void(0)'>USA</a>
          </li>
          <li>
            <a className='location' data-location='Europe' href='javascript:void(0)'>Europe</a>
          </li>
          <li>
            <a className='location' data-location='India' href='javascript:void(0)'>India</a>
          </li>
        </ul>
      </div>
       <div id='right'>
        <p>Connect</p>
          <div id='social'>
          <a className='social'>
            <span className='entypo-facebook'></span>
          </a>
          <a className='social'>
            <span className='entypo-twitter'></span>
          </a>
          <a className='social'>
            <span className='entypo-linkedin'></span>
          </a>
          <a className='social'>
            <span className='entypo-gplus'></span>
          </a>
          <a className='social'>
            <span className='entypo-instagrem'></span>
          </a>
         </div>
    </div>
  </div>
  </div>
      {/* Footer */}
        <FooterContainer />
</div>
    )
  }
}





