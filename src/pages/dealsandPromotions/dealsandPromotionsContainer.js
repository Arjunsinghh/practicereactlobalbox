import { getProductCountDeal} from '../../redux/action/productAction'
import { connect } from 'react-redux'
import dealsandPromotions from './dealsandPromotions'
const mapStoreToProps = state => ({
  productCount: state.product.products,
})
const mapDispatchToProps = dispatch => ({
  getProductCountDeal: ()=>dispatch(getProductCountDeal()),
  // applyFilters:(filter_string)=>dispatch(applyFilters(filter_string))
})

export default connect(mapStoreToProps, mapDispatchToProps)(dealsandPromotions)