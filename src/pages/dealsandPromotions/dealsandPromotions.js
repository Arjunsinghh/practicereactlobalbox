import React, { Component } from 'react'
import LoadingAnimation from '../../components/loadingAnimation'
import './stylesheets/dealsandPromotions.css'
import jumpTo,{go} from '../../modules/Navigation'


export default class dealsandPromotions extends Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
      this.props.getProductCountDeal()
  }
  render() {
    console.log('this.props.productCount',this.props.productCount);
    const logocompany = require('../../assets/images/GBX-silver-logo.png');

    return (
      <span>
          <a href="/home" className="GBX_logo">
            <img src={logocompany}/>
          </a>

          <div className="Our_heading container">
            <div className="sub_back text-left">
              <a onClick={() => jumpTo('landing-marketplace')} className="land_back"><i className="fas fa-arrow-left"></i><span> Back</span></a>
              <h2 className="shop_head">Deals and Promotions</h2>
            </div>
          </div>
          <div className="shopby shopby_fix">
            <div className="container">
                <div className="row margin_7">
                  <div className="col-sm-4">
                    <a ui-sref="marketplace({productType:'featured'})" className="recommend_box produc-pr_box">
                    <div className="recommend_txt">
                      <div className="recommend-icon"><i className="fas fa-shopping-basket"></i></div>
                      {this.props.productCount !== null ? this.props.productCount.reqestData.DiscountedProductCount :''}
                      <span className="recommend-border"></span>
                      <p className="recommend-below-title">Featured Products</p>
                    </div>
                    </a>
                  </div>
                  <div className="col-sm-4">
                    <a href="/landing-discounted" className="recommend_box discount-pr_box">
                      <div className="recommend_txt">
                        <div className="recommend-icon"><i className="fas fa-percentage"></i></div>
                        {this.props.productCount !== null ? this.props.productCount.reqestData.FeaturedProductCount :''}
                        <span className="recommend-border"></span>
                        <p className="recommend-below-title">Discounted Products</p>
                      </div>
                    </a>
                  </div>
                  <div className="col-sm-4">
                    <a href="#" className="recommend_box meg-brand-pr_box" title="Coming Soon">
                      <div className="recommend_txt">
                        <div className="recommend-icon"><i className="fas fa-weight-hanging"></i></div>
                        <span className="recommend-border"></span>
                        <p className="recommend-below-title">Mega Brands</p>
                      </div>
                    </a>
                  </div>
                </div>
            </div>
          </div>
      </span>
    )
  }
}





