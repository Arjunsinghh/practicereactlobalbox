import React, { Component } from 'react'
import FooterContainer from '../../components/footer/footerContainer'
import LoadingAnimation from '../../components/loadingAnimation'
import './stylesheets/login.css'
import jumpTo,{go} from '../../modules/Navigation'


export default class login extends Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    if (!this.props.products) {
      this.props.getAllProducts()
    }
  }
  render() {
    const { products, applyFilters } = this.props;
    var user_type = 'User';
    var country = 'India';
    var postal = 'India';
    var postal = 'India';
    const logocompany = require('../../assets/images/global-logo.png');
    const productCountCity = 20;
    const vendorCity = 20;
    const productsPostal = 20;
    const vendorPostal = 20;
    const hintButton = 0;
    return (
      <div className="pro_midcontianer mid_container">
        <div className="container">
          <div className="row topSpace">
            <div className="col-sm-6">
              <div className="ms1_left">
                <div className="ms1_logo">
                  <a href="/home">
                  <img src={logocompany} alt="img" className="img-responsive" />
                  </a>
                </div>
              </div>
            </div>
            <div className="col-sm-6">
              <div className="ms1_right">
                <h1>
                  THE WORLD'S ONLINE STORE
                </h1>
                <p>
                  Our Platform - Your Business; Your Success - Our Priority
                </p>
              </div>
            </div>
          </div>
          <div className="row login_wrapper">
              <div className="col-md-4 col-sm-4">
                <div className="alert alert-default list_view_login">
                  <h4>Before you Login/Register!</h4>
                  <br/>
                  <p>BY USING THE WEBSITE OR APP AND THE SERVICES OFFERED ON THE WEBSITE AND APP, YOU AGREE TO FOLLOW AND BE BOUND BY THESE TERMS AND CONDITIONS AND THE GLOBALBOX.WORLD PRIVACY POLICY WHICH ARE HEREBY INCORPORATED BY REFERENCE. IF YOU DO NOT WISH TO BE BOUND BY THESE TERMS AND CONDITIONS AND THE PRIVACY POLICY, YOU MAY NOT ACCESS OR USE THIS WEBSITE OR APP AND THE SERVICES OFFERED ON THE WEBSITE OR VIA THE APP.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><a href="termsCondition()"> TERMS OF USE </a></b></p>
                </div>
              </div>
              <div className="col-md-8 col-sm-8">
                <div className="panel panel-default">
                  <div className="panel-heading">
                    <h3 className="panel-heading headerLogin"><b><span></span> Login</b></h3>
                  </div>
                  <div className="panel-body DataBody">
                    <form className="form-horizontal login-form" role="form">
                      <input type="hidden" name="user_type" id="user_type" value="Buyer"/>
                      <div className="form-group">
                        <label className="col-md-4 control-label">E-Mail Address</label>
                        <div className="col-md-6">
                          <input id="email" type="email" className="form-control" name="email" required/>
                        </div>
                      </div>
                      <div className="form-group">
                        <label className="col-md-4 control-label">Password</label>
                        <div className="col-md-6">
                          <input id="password" type="password" className="form-control" name="password" required/>
                        </div>
                        {hintButton === 1 ?
                        <span>
                          <button type="button" className="btn btn-default">Password Hint</button>
                          
                        </span>
                        : ""}
                      </div>
                      <div className="form-group">
                        <div className="col-md-6 col-md-offset-4">
                          <div className="checkbox">
                            <label>
                                <input type="checkbox" name="remember"/> Remember Me
                            </label>
                          </div>
                        </div>
                      </div>
                      <div className="form-group">
                        <div className="col-md-6 col-md-offset-4">
                          <button type="submit" className="btn btn-primary login-btn">
                              <i className="fas fa-sign-in-alt"></i> Login
                          </button>
                          <a className="btn btn-link" href="reset-password">Forgot Your Password?</a>
                        </div>
                      </div>
                    
                    </form>

                  </div>
                </div>
              </div>
            </div>
        </div>
        
        {/* Footer */}
          <FooterContainer />
          
      </div>

    )
  }
}





