import React, { Component,useState } from 'react'
import LoadingAnimation from '../../components/loadingAnimation'
import jumpTo,{go} from '../../modules/Navigation'
import './stylesheets/landingMarketplace.css'


export default class landingMarketplace extends Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    this.props.getProductCount()
  }


  render() {
    const logocompany = require('../../assets/images/GBX-silver-logo.png');
    const logocity = require('../../assets/images/store.png');
    const logoCountCity = require('../../assets/images/shop.png');
    console.log('this.props',this.props);

    return (
      <span>
          <a href="/home" className="GBX_logo">
            <img src={logocompany}/>
          </a>

          <div className="Shop_heading container">
              <h2 className="shopData shop_head shopby_head ">Find Your Product</h2>
          </div>
          <div className="categories_search_bar">
            <div className="container">
              <div className="wht_bx">
                <div className="flex_div">
                  <form className="navbar-form">
                    <a className="voice_serch" title="Search by Voice Coming Soon"><i className="fas fa-microphone"></i></a>
                    <input className="form-control" placeholder="Search by Keyword" type="text" />
                  </form>
                  <div className="btns_div">
                    <button type="button" className="btn blueBtn">Products <span className="play_icon"><i className="fas fa-play"></i></span></button>
                    <button type="button" className="btn light_green_btn">Suppliers <span className="play_icon"><i className="fas fa-play"></i></span></button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="shopby">
              <div className="container">
                  <div className="row margin_7">
                      <div className="col-sm-3">
                          <a onClick={() => jumpTo('/landing-shop')} className="shop_box ">
                              <div className="land_content-box">
                                  <div className="shop-icon"><img src={logoCountCity} /></div>
                                  {this.props.productCount !== null ? this.props.productCount.cityCount :''}
                                  <span className="shop-border"></span>
                                  <p className="shop-below-title">Shop in {this.props.productCount !== null ? this.props.productCount.city :''}</p>
                              </div>    
                          </a>
                      </div>
                      <div className="col-sm-3">
                          <a onClick={() => jumpTo('/landing-province')} className="shop_box keyword_box">
                              <div className="land_content-box">
                                  <div className="shop-icon"><i className="fas fa-store"></i></div>
                                  {this.props.productCount !== null ? this.props.productCount.StateProductCount :''}
                                  <span className="shop-border"></span>
                                  <p className="shop-below-title">Shop in {this.props.productCount !== null ? this.props.productCount.state :''}</p>
                              </div>
                          </a>
                      </div>
                        <div className="col-sm-3">
                          <a href="" className="shop_box avail_box">
                            <div className="land_content-box">
                              <div className="shop-icon"><i className="fas fa-globe-asia"></i></div>
                              {this.props.productCount !== null ? this.props.productCount.countryCount :''}
                              <span className="shop-border"></span>
                              <p className="shop-below-title">Available  in {this.props.productCount !== null ? this.props.productCount.countryName :''}</p>
                            </div>
                          </a>
                        </div>
                        <div className="col-sm-3">
                          <a onClick={() => jumpTo('/landing-deal-promotion')} className="shop_box recomm_box">
                            <div className="land_content-box">
                              <div className="shop-icon"><i className="far fa-check-circle"></i></div>
                              {this.props.productCount !== null ? this.props.productCount.dealCount :''}
                              <span className="shop-border"></span>
                              <p className="shop-below-title mendations_txt">Deals and Promotions</p>
                            </div>
                          </a>
                        </div>
                  </div>
                  <div className="row margin_7">
                  <div className="col-sm-3">
                   <a onClick={() => jumpTo('/advance-search')} className="shop_box adv_box ">
                      <div className="land_content-box">
                        <div className="shop-icon"><i className="fas fa-search-plus"></i></div>
                        <span className="shop-border"></span>
                        <p className="shop-below-title">Advanced Search</p>
                      </div>
                    </a>
                  </div>      
                  
                  <div className="col-sm-3">
                    <a href="#" className="shop_box cate_box toggle-button" data-toggle="modal" data-target="#myModal">
                      <div className="land_content-box">
                        <div className="shop-icon"><i className="fas fa-sitemap"></i></div>
                        <span className="shop-border"></span>
                        <p className="shop-below-title">Categories</p>
                      </div>
                    </a>
                  </div>
                  <div className="col-sm-3">
                    <a onClick={() => jumpTo('/landing-market-segment')} className="shop_box market_box">
                      <div className="land_content-box">
                        <div className="shop-icon market_sg"><img src={logocity} /></div>
                        <span className="shop-border"></span>
                        <p className="shop-below-title">Market Segments</p>
                      </div>
                    </a>
                  </div>
                  <div className="col-sm-3">
                    <a onClick={() => jumpTo('/marketplace-home')} className="shop_box home-page_box">
                      <div className="land_content-box">
                        <div className="shop-icon"><i className="fas fa-home"></i></div>
                        <span className="shop-border"></span>
                        <p className="shop-below-title">Home Page</p>
                      </div>
                    </a>
                  </div>        
                </div>  
              </div>
          </div>
          {/* Home Page Popup  css Strat */}
          <div className="modal fade Categories_Popup" id="myModal" role="dialog">
            <div className="modal-dialog modal-lg" role="document">
              <div className="modal-content">
                <div className="modal-content">
                  <div className="modal-header blue_btn">
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close"><i className="far fa-times-circle"></i></button>
                    <h4 className="modal-title withoutuser uprcase">All Categories<span className="icon_spn blue"></span> </h4>
                  </div>
                  <div className="modal-body">
                    <div className="heading_control">
                      <div className="panel_heading">
                        <span className="accordion_txt"><a></a></span>
                        <a className="panel_title"><i className="fas fa-list-ul"></i></a>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* Home Page Popup  css Closed */}
      </span>
    )
  }
}





