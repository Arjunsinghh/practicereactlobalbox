import { getProductCount } from '../../redux/action/productAction'
import { connect } from 'react-redux'
import landingMarketplace from './landingMarketplace'

const mapStoreToProps = state => ({
  productCount: state.product.products,
})
const mapDispatchToProps = dispatch => ({
  getProductCount: ()=>dispatch(getProductCount()),
  // applyFilters:(filter_string)=>dispatch(applyFilters(filter_string))
})

export default connect(mapStoreToProps, mapDispatchToProps)(landingMarketplace)