import React, { Component } from 'react'
import FooterContainer from '../../components/footer/footerContainer'
import './stylesheets/termsCondition.css'
import jumpTo,{go} from '../../modules/Navigation'


export default class termsCondition extends Component {

  render() {
    
    return (
      <div className="pro_midcontianer mid_container">
        <div className="container">
          <div className="col-md-12 terms">
            <h2 className="dark-grey"><b>TERMS AND CONDITIONS</b></h2>
            <br/>
            <p><b>1. General Prohibitions</b></p>
            <p><b>2. Artifacts</b></p>
            <p><b>3. Currency and Stamps</b></p>
            <p><b>4. Contracts and Tickets</b></p>
            <p><b>5. Credit / Debit Cards and Financial Instruments</b></p>
            <p><b>6. Illicit Drugs and Associated Paraphernalia </b></p>
            <p><b>7. Ethnically and Racially Offensive Material</b></p>
            <p><b>8. Event Ticket Resale Policy</b></p>
            <p><b>9. Faces, Names and Signatures</b></p>
            <p><b>10. Firearms, Ammunition, Weapons, Explosives</b></p>
            <p><b>11. Alcohol, Food and Beverages</b></p>
            <p><b>12. Tobacco Products</b></p>
            <p><b>13. Government Identifications, Licenses and Uniforms</b></p>
            <p><b>14. Hazardous Materials</b></p>
            <p><b>15. Human Parts and Remains</b></p>
            <p><b>16. Invoices</b></p>
            <p><b>17. Mailing Lists and Personal Information</b></p>
            <p><b>18. Non-Transferable Items</b></p>
            <p><b>19. Police-Related Items</b></p>
            <p><b>20. Pornographic Materials and Adult Products</b></p>
            <p><b>21. Medical Drugs and Devices</b></p>
            <p><b>22. Circumvention Devices and Hacking Equipment</b></p>
            <p><b>23. Pirated Software</b></p>
            <p><b>24. Stolen Property</b></p>
            <p><b>25. Unauthorized Copies of Intellectual Property</b></p>
            <p><b>26. Used Clothing and Cosmetics</b></p>
            <p><b>27. Wildlife and Animal Products</b></p>
            <p><b>28. Sanctioned and Prohibited Items</b></p>
            <br/>
            <h3><b>1. GENERAL PROHIBITIONS:</b></h3>
            <p>
              1.1 You may not post or sell any item that is restricted or prohibited by a federal, state or local law in any country or jurisdiction. Please be aware that the www.GlobalBox.world and www.gbx.world websites (each a "Site") function as global marketplaces, thus the selling or posting of items may be prohibited because of laws outside of the jurisdiction where you reside. Below, we have listed some categories of prohibited or restricted items. However, this list is not exhaustive; you, as the seller, are responsible for ensuring that you are not posting an item that is prohibited by law in any jurisdiction. 
            </p>
            <p>1.2 GlobalBox.world has chosen to also prohibit the posting of other categories of items which       may not be restricted or prohibited by law but are nonetheless controversial including: 
            (a) Items that encourage illegal activities (e.g. lock pick tools, synthetic urine for cheating drug tests); 
            (b) Items that are racially, religiously or ethnically derogatory, or that promote hatred, violence, racial or religious intolerance; 
            (c) Giveaways, lotteries, raffles, or contests; 
            (d) Stocks, bonds, investment interests, and other securities; 
            (e) Pornographic materials or items that are sexual in nature;
            (f) Items that do not offer a physical product for sale, such as electronic books and other digital goods.</p>
            <p>
            1.3 GlobalBox.world, in its sole and exclusive discretion, reserves the right to impose additional restrictions and prohibitions.
            </p>
            <br/>
            <h3><b>2. ARTIFACTS </b></h3>
            <p>2.1 Artifacts, cultural relics, historical grave markers, and related items are protected under the laws of Hong Kong, the PRC and other jurisdictions, and may not be posted or sold through the Site. </p>
            <p>2.2 The trade of cultural relics (including personal collection relics), gold, silver, other precious metals, rare animals and their products from the PRC to foreign (non-PRC) parties is strictly prohibited under PRC law and is accordingly prohibited on the Site.</p>
            <br/>
            <h3><b>3. CURRENCY AND STAMPS</b></h3>
            <p>3.1 GlobalBox.world strictly forbids the sale and purchase of currency, coins, bank notes, bonds, money orders, currency in digital or any intangible form (e.g. crypto-currency) and other securities, as well as the equipment and materials used to produce such items. </p>
            <p>3.2 Counterfeits of the identified articles in 3.1, legal tenders and stamps are strictly prohibited. </p>
            <p>3.3 Reproductions or replicas of coins as collectible items must be clearly marked with the word "COPY", "REPRODUCTION" or "REPLICA" and comply with all relevant local laws. </p>
            <br/>
            <h3><b>4. CONTRACTS AND TICKETS </b></h3>
            <p>4.1 You are responsible for ensuring that your transaction is lawful and not in violation of any contractual obligation. Before posting an item on the Site, you should carefully read any contracts that you have entered into that might limit your right to sell your item on the Site. Some items, such as airline tickets, have terms printed on the item that may limit your ability to sell that item. In other cases, such as when you are distributing a company's products, you may have signed a separate contract restricting your ability to market the product.  </p>
            <p>4.2 GlobalBox.world does not search for items that may raise these types of issues, nor can it review copies of private contracts, or adjudicate or take sides in private contract disputes. However, we want you to be aware that posting items in violation of your contractual obligations could put you at risk with third parties. GlobalBox.world therefore urges that you not list any item until you have reviewed any relevant contracts or agreements, and are confident you can legally sell it on the Site.  </p>
      <p>4.3 If you have any questions regarding your rights under a contract or agreement, we strongly recommend that you contact the company with whom you entered into the contract and/or consult with an attorney. </p>
      <br/>
      <h3><b>5. CREDIT / DEBIT CARDS AND FINANCIAL INSTRUMENTS </b></h3>
      <p>5.1 Credit and debit cards cannot lawfully be transferred from one person to another, and therefore such items may not be listed on the Site. </p>
      <p>5.2 Card readers are prohibited from being listed on the Site. </p>
      <p>5.3 Listings that offer the sale or buying of gold, silver and other precious metals (not including jewelry) are prohibited. </p>
      <p>5.4 The listing or sale of stocks, bonds, credit, investment interests, or other securities is strictly prohibited. </p>
      <p>5.5 GlobalBox.world prohibits listings that offer financial services, including money transfers, issuing bank guarantees and letters of credit, loans, fundraising and funding for personal investment purposes etc. </p>
      <br/>
      <h3><b>6. ILLICIT DRUGS AND ASSOCIATED PARAPHERNALIA </b></h3>
      <p>6.1 The listing or sale of narcotics, steroids, poppy seeds, poppy seed products or other controlled substances (including all drugs listed in Schedules I, II, III, IV or V of the Uniform Controlled Substances Act, 21 U.S.C. 801 et seq.) is strictly forbidden on the Site.  </p>
      <p>6.2 GlobalBox.world expressly forbids any and all posting of psychotropic drugs and narcotics. </p>
      <p>6.3 The listing or sale of drug paraphernalia, including all items that are primarily intended or designed for use in manufacturing, concealing, or using a controlled substance is strictly forbidden on the Site. Such items include, but are not limited to those items used for the ingestion of illicit substances including pipes such as water pipes, carburetor pipes, chamber pipes, ice pipes, bongs etc. </p>
      <br/>
      <h3><b>7. ETHNICALLY AND RACIALLY OFFENSIVE MATERIAL </b></h3>
      <p>7.1 Postings that are ethnically or racially offensive are prohibited on the Site. Sellers and purchasers must ensure that any wording used portrays appropriate sensitivity to those who might read it in their postings, and when they are offering or purchasing potentially offensive items or services.  </p>
      <p>7.2 Occasionally, if materials are of historical value or integral to the item (such as a book title), members may use offensive words and phrases such as "Yang Guizi" in the subject and description of a posting. GlobalBox.world reserves the sole discretion to decide the removal of such items and encourages all members to treat others as they themselves would like to be treated. </p>
      <p>7.3 GlobalBox.world generally prohibits such materials promoting Nazism, the KKK, etc. </p>
      <br/>
      <h3><b>8. EVENT TICKET RESALE POLICY </b></h3>
      <p>8.1 GlobalBox.world allows the listing of tickets to performance, sporting and entertainment events to the extent permitted by law. However, as a ticket seller, you are responsible for ensuring that your particular transaction does not violate any applicable law or the terms on the ticket itself.  </p>
      <br/>
      <h3><b>9. FACES, NAMES AND SIGNATURES </b></h3>
      <p>9.1 Items containing the likeness, image, name, or signature of another person are prohibited, unless the products were made or authorized by the person whose likeness, image, name or signature has been used.  </p>
      <br/>
      <h3><b>10. FIREARMS, AMMUNITION, WEAPONS, EXPLOSIVES </b></h3>
      <p>10.1 The posting of, offering for sale, or offering for purchase of any arms, ammunition, military ordnance, weapons (including explosive weapons), and/or integral or essential parts and components is strictly prohibited. Such activity can result in your account being delisted.</p>
      <p>10.2 GlobalBox.world does not permit the posting, offering for sale, or offering of purchase of air guns, BB guns, paintball guns, harpoons, spear guns, or any other weapons that may discharge a projectile containing any gas, chemicals, or explosive substances. Replica, "look-alike", and imitation products of the above items under certain circumstances will be permitted only upon the express approval of GlobalBox.world.  </p>
      <p>10.3 The posting of explosives, fireworks and related ignition and detonation equipment is strictly prohibited. </p>
      <p>10.4 Any service, instruction, processing, or aid for producing any biological, chemical, or nuclear weapons, or any other Weapons of Mass Destruction (WMD) or known associated agents is strictly prohibited by international law and is accordingly prohibited on the Site. Any violation of this policy will result in the notification of government authorities by GlobalBox.world and your account being delisted.  </p>
      <p>10.5 Knives and other cutting instruments will in most cases be permitted to be listed. However, switchblade knives, gravity knives, bladed handheld devices, lock blade knives, knives with blades over 10cm in length, knives with fullers (blood grooves) and disguised knives are not permitted to be listed. </p>
      <p>10.6 Batons, stun guns, crossbows, knuckledusters, pepper spray and other weapons are prohibited from being listed on the Site.GlobalBox.world maintains discretion over what items are appropriate and may cause removal of a listing that it deems as a weapon. </p>
      <br/>
      <h3><b>11. ALCOHOL, FOOD AND BEVERAGES </b></h3>
      <p>11.1 The listing of alcohol is prohibited on the Site. </p>
      <br/>
      <h3><b>12. TOBACCO PRODUCTS  </b></h3>
      <p>12.1 The posting of tobacco products, including but not limited to cigars, cigarettes, cigarette tobacco, pipe tobacco, hookah tobacco, chewing tobacco and tobacco leaf is forbidden on the Site.</p>
      <p>12.2 The posting of electronic cigarettes and accessories is prohibited.</p>
      <p>12.3 Equipment used in the processing of tobacco and manufacture of tobacco products is prohibited on the Site. </p>
      <br/>
      <h3><b>13. GOVERNMENT IDENTIFICATIONS, LICENSES AND UNIFORMS  </b></h3>
      <p>3.1 The following items are not permitted to be listed on the Site:  
      (a) Items that claim to be, or appear similar to, official government identification documents or licenses, such as birth certificates, driving licenses, visas or passports. Furthermore, completed applications for such documents containing personal information may not be listed.  
      (b) Fake identification cards or any items that are designed for the production of such cards (e.g. ID card holograms). 
      (c) Articles of clothing or identification that claim to be, or appear similar to, official government uniforms. 
      (d) Military decorations, medals and awards, in addition to items with substantially similar designs. </p>
      <br/>
      <h3><b>14. HAZARDOUS AND DANGEROUS MATERIALS </b></h3>
      <p>14.1 The posting of any chemical products on the Site is prohibited.  </p>
      <p>14.2 The posting, offering for sale, or offering for purchase of hazardous or dangerous materials (such as the categories of dangerous goods as defined under the International Maritime Dangerous Goods Code) is forbidden on the Site.  </p>
      <p>14.3 The posting of any products containing harmful substances (e.g. toys containing lead paint) is forbidden on the Site. </p>
      <p>14.4 Automotive airbags are expressly forbidden on the Site due to containing explosive materials. </p>
      <p>14.5 Asbestos materials and/or products containing asbestos are prohibited.</p>
      <br/>
      <h3><b>15. HUMAN PARTS AND REMAINS </b></h3>
      <p>15.1 GlobalBox.world prohibits the listing of any humans, the human body, or any human body part. Examples of such prohibited items include, but are not limited to: organs, bones, blood, sperm and eggs.  </p>
      <p>15.2 Skulls and skeletons are also forbidden</p>
      <p>15.3 Items made of human hair, such as wigs for commercial uses, are permitted.</p>
      <br/>
      <h3><b>16. INVOICES </b></h3>
      <p>16.1 The listing or sale of any form of invoices or receipts (including blank, pre-filled, or value added invoices or receipts), is strictly prohibited on the Site.  </p>
      <br/>
      <h3><b>17. MAILING LISTS AND PERSONAL INFORMATION </b></h3>
      <p>17.1 The posting or sale of bulk email or mailing lists that contain personally identifiable information including names, addresses, phone numbers, fax numbers and email addresses, is strictly prohibited.  </p>
      <p>17.2 Software or other tools which are designed or used to send unsolicited commercial email (i.e. "spam") are prohibited.  </p>
      <br/>
      <h3><b>18. NON-TRANSFERABLE ITEMS </b></h3>
      <p>18.1 Non-transferable items may not be posted or sold through the Site. Many items including lottery tickets, airline tickets and some event tickets may not be resold or transferred. </p>
      <br/>
      <h3><b>19. POLICE-RELATED ITEMS </b></h3>
      <p>19.1 The posting of law enforcement badges or official law enforcement equipment from any public authority, including badges issued by the government of any country, is strictly prohibited.  </p>
      <p>19.2 There are some police items that may be listed on the Site, provided they observe the following guidelines:  
      (a) Authorized general souvenir items, such as hats, mugs, pins, pens, buttons, cuff links, T-shirts, money clips that do not resemble badges, and paperweights that do not contain badges.  
      (b) Badges that are clearly not genuine or official (e.g. toy badges).  
      (c) Historical badges that do not resemble modern law enforcement badges, provided that the item description clearly states that the badge is a historical piece at least 75 years old or issued by an organization which no longer exists.  </p>
      <p>19.3 Police uniforms may not be posted unless they are obsolete and in no way resemble current issue police uniforms. This fact must be clearly stated within the posting description.  </p>
      <br/>
      <h3><b>20. PORNOGRAPHIC MATERIALS AND ADULT PRODUCTS </b></h3>
      <p>20.1 The posting or sale of pornographic materials is strictly prohibited, as it violates laws in many countries. While pornography is difficult to define and standards vary from nation to nation, </p>
      <p>20.2 GlobalBox.world strictly prohibits items depicting exploitation of minors, bestiality, rape sex, incest or sex with graphic violence or degradation. </p>
      <p>20.3 In determining whether listings or information should be removed from the Site, we consider the overall content of the posting, including photos, pictorials and text. </p>
      <p>20.4 GlobalBox.world does not permit the sale of SM products. Novelty sexually explicit items, including various types of toys, collectibles, or food items in the shape of genitalia and primarily intended for adult buyers are permitted in the appropriate category. </p>
      <br/>
      <h3><b>21. MEDICAL DRUGS AND DEVICES </b></h3>
      <p>21.1 The posting of all medical drugs, regardless whether classified as prescription, over-the-counter (OTC), vaccine or other types is prohibited on the Site.  </p>
      <p>21.2 The listing or sale of sexual enhancement and weight loss supplements is prohibited.  </p>
      <p>21.3 The posting of all medical devices is prohibited.  </p>
      <p>21.4 Veterinary drugs and devices cannot be listed on the Site.  </p>
      <p>21.5 Listings that offer medical or healthcare services, including services for medical treatment, rehabilitation, vaccination, health checks, psychological counselling, dietetics, plastic surgery, massage etc. are expressly prohibited on the Site. </p>
      <br/>
      <h3><b>22. REPLICA AND COUNTERFEIT ITEMS </b></h3>
      <p>22.1 Listing of counterfeits, non-licensed replicas, or unauthorized items, such as counterfeit designer garments, watches, handbags, sunglasses, or other accessories, are strictly limited on the Site.  </p>
      <p>22.2 If the products sold bear the name or logo of a company, but did not originate from or were not endorsed by that company, such products are limited in terms of their sale on the site.  </p>
      <p>22.3 Postings of branded products are permitted if a certificate of authorization has been issued by the brand owner.  </p>
      <p>22.4 Postings offering to sell or purchase replicas, counterfeits or other unauthorized items may be subject to removal by GlobalBox.world. Repeated postings of counterfeit or unauthorized items shall result in the immediate suspension of your membership. To learn more about GlobalBox.world’s policy on replica and counterfeit items please contact us. </p>
      <br/>
      <h3><b>23. HACKING EQUIPMENT</b></h3>
      <p>23.1 Descramblers or other items that can be used to gain unauthorized access to television programming (such as satellite and cable TV), internet access, telephone, data or other protected, restricted, or premium services are prohibited. Stating the item is for educational or test purposes will not legitimize a product that is otherwise inappropriate. Some examples of items which are not permitted include smart cards and card programmers, descramblers, DSS emulators and hacking software. </p>
      <p>23.2 Similarly, information on "how to" descramble or gain access to cable or satellite television programming or other services without authorization or payment is prohibited. GlobalBox.world's policy is to prohibit any encouragement of this type of activity. </p>
      <p>23.3 Any and all unauthorized circumvention or hacking devices not included in the above are also strictly prohibited. </p>
      <p>23.4 Devices designed to intentionally block, jam or interfere with authorized radio communications, such as cellular and personal communication services, police radar, global positioning systems (GPS) and wireless networking services (Wi-Fi) are prohibited. </p>
      <br/>
      <h3><b>24. SOFTWARE  </b></h3>
      <p>24.1 Academic software: 
      (a) Academic software is software sold at discounted prices to students, teachers, and employees of accredited learning institutions.  
      (b) On the Site, please do not list any academic software unless you are so authorized. Postings violating GlobalBox.world's academic software policy may be deleted prior to publication.  
      (c) For postings of academic software on behalf of an authorized educational reseller or an educational institution, such licensure must be stated conspicuously in the listings. A certificate of authorization issued by the authorized educational reseller (or the educational institution) must also be provided to GlobalBox.world.  </p>
      <p>24.2 OEM software: 
      (a) Do not list "OEM" or "bundled" copies of software on the Site unless you are selling it with computer hardware. Original Equipment Manufacturer (OEM), or bundled software, is software that is obtained as part of the purchase of a new computer. OEM software licenses usually prohibit the purchaser from reselling the software without the computer or, in some cases, without any computer hardware.  </p>
      <br/>
      <h3><b>25. SPY EQUIPMENT </b></h3>
      <p>25.1 The listing or sale of spy equipment is not permitted on the Site. </p>
      <p>25.2 The listing of devices used for interception of wire, oral and electronic communications is prohibited. </p>
      <br/>
      <h3><b>26. STOLEN PROPERTY</b></h3>
      <p>26.1 The posting or sale of stolen property is strictly forbidden on the Site, and violates international law. Stolen property includes items taken from private individuals, as well as property taken without authorization from companies or governments.  </p>
      <p>26.2 GlobalBox.world supports and cooperates with law enforcement efforts involving the recovery of stolen property and the prosecution of responsible individuals. If you are concerned that the images and/or text in your item description have been used by another Site user without your authorization, or that your intellectual property rights have been violated by such user, please contact our service team at info@GlobalBox.world.  </p>
      <br/>
      <h3><b>27. TRANSIT RELATED ITEMS  </b></h3>
      <p>27.1 The following items related to the commercial airline and public transportation industries may not be listed on the Site:  
      (a) Any article of clothing or identification related to transportation industries, including but not limited to, commercial airline pilot uniforms, flight attendant uniforms, airport service personnel uniforms, uniforms related to railway industries, and uniforms of security personnel of public transport industries. Vintage clothing related to commercial airlines or other public transport may be listed on the Site provided that the item description clearly states that the item is at least 10 years old, is no longer in use by the airline or other public transport authority and does not resemble any current uniform.  
      (b) Manuals or other materials related to mass commercial public transportation, including safety manuals published by commercial airlines or entities operating subways, trains or buses. Such items may only be listed if the description clearly states that the material is obsolete and no longer in use by the airline or other transit authority.  
      (c) Any official, internal, or non-public documents.  </p>
      <br/>
      <h3><b>28. UNAUTHORIZED COPIES OF INTELLECTUAL PROPERTY </b></h3>
      <p>28.1 The listing or sale of unauthorized (pirated, duplicated, backup, bootleg etc.) copies of software programs, video games, music albums, movies, television programs, photographs or other protected works is forbidden on the Site.  </p>
      <br/>
      <h3><b>29. USED CLOTHING AND COSMETICS </b></h3>
      <p>29.1 Used undergarments may not be listed or sold on the Site. Other used clothing may be listed, so long as the clothing has been thoroughly cleaned. Postings that contain inappropriate or extraneous descriptions will be removed.  </p>
      <p>29.2 The listing or sale of used cosmetics is prohibited on the Site.  </p>
      <br/>
      <h3><b>30. WILDLIFE AND ANIMAL PRODUCTS  </b></h3>
      <p>30.1 The listing or sale of any animal (including any animal parts which may include pelts, skins, internal organs, teeth, claws, shells, bones, tusks, ivory and other parts) protected by the Convention on International Trade in Endangered Species of Wild Fauna and Flora (CITES) or any other local law or regulation is strictly forbidden on the Site.  </p>
      <p>30.2 The listing or sale of products made with any part of and/or containing any ingredient derived from sharks or marine mammals is prohibited on the Site. </p>
      <p>30.3 The listing or sale of products made from cats, dogs, bears, and any processing equipments.  </p>
      <p>30.4 The listing or sale of any live animal is prohibited on the Site. </p>
      <br/>
      <h3><b>31. TEXTILE QUOTAS</b></h3>
      <p>31.1 The offering for sale or purchase of textile quotas is prohibited on the Site. </p>
      <br/>
      <h3><b>32. SANCTIONED AND PROHBITED ITEMS</b></h3>
      <p>32.1 Products prohibited by laws, regulations, sanctions and trade restrictions in any relevant country or jurisdiction worldwide are strictly forbidden.</p>
      <br/>
      <br/>
      <h3><b>IMPORTANT:</b></h3>
      <p>This list should not be considered exhaustive in nature and shall be updated on a continuous basis. If you are unsure about the product you wish to list with the Site in regard to its appropriateness or legality, please contact our customer service department.</p>
      <p>GlobalBox.world reserves the right to prevent the posting of, or the removal of, any product that does not adhere to the stated policies, guidelines and regulations. After repeat offences the user’s account may be blocked, and the user’s access to the site may be entirely denied.</p>

      
                  
         </div>
        </div>
      {/* Footer */}
        <FooterContainer />
      </div>
    )
  }
}





