import React, { Component } from 'react'
import FooterContainer from '../../components/footer/footerContainer'
import './stylesheets/home.css'
import jumpTo, { go } from '../../modules/Navigation'


export default class home extends Component {
  constructor(props) {
    super(props);
  }
  cookieClick =(value)=>{
    localStorage.setItem('conditionDataHeader', value)
}

  render() {


    const rlogo = require("../../assets/images/rlogo.png");
    const marketplace = require("../../assets/images/marketplace.jpeg");
    const service = require("../../assets/images/service.jpeg");
    const generic_facebook = require("../../assets/images/generic_facebook.jpg");
    const promotional = require("../../assets/images/promotional.jpeg");
    const conditionDataHeader = localStorage.getItem('conditionDataHeader');
  
    const ConditionData = "";
    return (
      <span>
        <div className="mid_container pro_midcontianer">
        <div className="top_bannerData">
        <img src={generic_facebook} title="banner" className="ImageBanner" alt="top_banner"/>
        <h1 className="centeredData">GlobalBox.world</h1>
        <h1 className="centered">The World's Online Store</h1>
        </div>
        <div className="containerData">
        <img src={rlogo} title="logo" className="mainLogo"/>
        </div>

        <div className="container">
        <h1 className="main_heading">Welcome to the IoC - Internet of Commerce</h1>
        <div className="categ_boxes">
        <div className="row">
        <div className="col-sm-12">
        <div className="col-sm-4">
        <a><img src={marketplace} className="ImageData" alt="Snow"/>
        <h3 className="centeredMain">Find Your Product</h3></a>
        </div>
        <div className="col-sm-4">
        <a><img src={service} className="ImageData" alt="Forest"/>
        <h3 className="centeredMain">Find Your Service</h3></a>
        </div>
        <div className="col-sm-4">
        <a><img src={promotional} className="ImageData" alt="Mountains"/>
        <h3 className="centeredMain">Find Your Promotion</h3></a>
        </div>

        </div>
        </div>
        </div>

        </div>
        <FooterContainer/>
           
       {conditionDataHeader !== "1"?
        
        <div className="footer_label">
        <p className="contentCookies">We use cookies to better understand how you use our site, and to improve your overall experience.This includes personalizing content-advertising.To continue using our site you need to agree with our revised <a href = "/privacy-policy" onClick={() => jumpTo('/privacy-policy')}>privacy policy</a> and <a href = "/terms-condition" onClick={() => jumpTo('/terms-condition')}>Terms of Use</a> </p>
           <button type="button" className="btn btn-primary" onClick={() => this.cookieClick(1)} >I Agree</button>&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" className="btn btn-danger" onClick={()=>alert("get out")}>Disagree</button>
        </div>
         :""}
         </div>
      </span>
    )
  }
}





