import { getSigninData } from '../../redux/action/signinAction'
import { connect } from 'react-redux'
import signup from './signup'
const mapStoreToProps = state => ({
  signData: state.signin.signin_data,
})
const mapDispatchToProps = dispatch => ({
  getSigninData: ()=>dispatch(getSigninData()),
  // applyFilters:(filter_string)=>dispatch(applyFilters(filter_string))
})

export default connect(mapStoreToProps, mapDispatchToProps)(signup)