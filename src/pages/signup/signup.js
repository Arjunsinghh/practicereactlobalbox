import React, { Component } from 'react'
import FooterContainer from '../../components/footer/footerContainer'
import LoadingAnimation from '../../components/loadingAnimation'
import './stylesheets/signup.css'
import jumpTo,{go} from '../../modules/Navigation'
import ReCAPTCHA from "react-google-recaptcha"
import {
  validateExistence,
  validateEmail,
  validateLength,
  validateLowerCase,
  validateUpperCase
} from './utils/validation'


const INPUT_CONFIG = [
  {
    name: "primary_affiliate",
    validations: [validateExistence]
  },
  {
    name: "firstname",
    validations: [validateExistence]
  },
  {
    name: "firstname",
    validations: [validateExistence]
  },
  {
    name: "email",
    validations: [validateExistence, validateEmail]
  },
  {
    name: "email_confirm",
    validations: [validateExistence, validateEmail]
  },
  {
    name: "password",
    validations: [validateExistence]
  },
  {
    name: "password_confirmation",
    validations: [validateExistence]
  },
  {
    name: "password_hint",
    validations: [validateExistence]
  },
  {
    name: "mobile_no",
    validations: [validateExistence]
  },
]

function onChange(value) {
  console.log("Captcha value:", value);
}

export default class signup extends Component {
  constructor(props) {
    super(props);
    this.state = {
        menu: false
      };
    this.toggleMenu = this.toggleMenu.bind(this);
  }
  componentDidMount() {
      this.props.getSigninData()
  }
  toggleMenu(){
    this.setState({ menu: !this.state.menu })
  }

  getValue(event){
    console.log('value',event.target.value);
  }

  render() {
    const  pageDetails  = this.props.signData;

    var userType = 'User';
    var registerType = 'User';
   
    const optionsday = [];
    const optionsmonth = [];
    const optionsyear = [];
    const countryData = [];
    const days = ['01','02','03','04','05','06','07','08','09',10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31];
    const month = ['01','02','03','04','05','06','07','08','09',10,11,12];
    const years = [];
    var objday = {}
    var objmonth = {}
    var objyear = {}
    var countryDataMain = {}

    const show = (this.state.menu) ? "show" : "" ;

    var d = new Date();
    var n = d.getFullYear();
    var currentyear = n - 19;

    for(var i = currentyear; i>=1920; i--) {
      var objyear = {};

      objyear['value'] = i;
      objyear['label'] = i;
      optionsyear.push(objyear);
    }

    for(var i = 0; i < days.length; i++) {
      var objday = {};

      objday['value'] = days[i];
      objday['label'] = days[i];
      optionsday.push(objday);
    }

    for(var i = 0; i < month.length; i++) {
      var objday = {};

      objday['value'] = month[i];
      objday['label'] = month[i];
      optionsmonth.push(objday);
    }    

    if(pageDetails !== null){
      var DefaultAffiliate = pageDetails.DefaultAffiliate;
      var CountryName = pageDetails.CountryCode;
      var codeNameone = pageDetails.calling_code;
      var codeimgone = pageDetails.flagURL+pageDetails.country_code+'.png';
      for(var i = 0; i < CountryName.length; i++) {
        var countryDataMain = {};
        countryDataMain['name'] = CountryName[i].CountryName;
        countryDataMain['flagurl'] = pageDetails.flagURL;
        countryDataMain['flagname'] = CountryName[i].CountryFlag;
        countryDataMain['phonecode'] = CountryName[i].CountryPhoneCode;
        countryData.push(countryDataMain);
      }
    }else{
      var DefaultAffiliate = 'AAA0001';
      var CountryName = null;
      var codeNameone = '';
      var codeimgone = '';
    }

    
    return (
        <div className="pro_midcontianer mid_container">
          <div className="main_content">
            <div className="form_sec">
              <div className="container">
                <div className="form_toptxt">
                  <h2 className="form_head"><a href="#" className="Darkblue_btn"><i className="far fa-user"></i></a>Register - {registerType}</h2>
                  <h3 className="form_subhead">All fields with an "<span className="txt_red">*</span>" are required.</h3>
                  {userType !=='Buyer' && userType !=='Advertiser' ? 
                  <p className="form_summary">
                    <span className="txt_red">Important!</span><b> If you DO NOT have an Affiliate Number provided to you by one of our GlobalBox Affiliates, please enter <span className="txt_red">{DefaultAffiliate}</span> in the Primary Affiliate field below.</b>
                  </p>
                  : ""}
                </div>
                <form className="form-horizontal" role="form" INPUT_CONFIG={INPUT_CONFIG}>
                <div className="Register_form wht_box">
                {userType !=='Buyer' && userType !=='Advertiser' ? 
                  <div className="row margin_10">
                    <div className="col-lg-4 col-sm-6">
                      <div className="form-group">
                        <label className="label_txt required_fld">Primary Affiliate<span className="txt_red">*</span></label>
                        <input id="affiliate_code" type="text" value="" className="form-control title_check border-error" placeholder={"Default: "+DefaultAffiliate}
                          name="primary_affiliate"  required />
                          <p></p>
                      </div>
                    </div>
                  </div>
                  : ""}
                  <div className="row margin_10">
                    <div className="col-lg-4 col-sm-6">
                      <div className="form-group">
                        <label className="label_txt required_fld">First Name<span className="txt_red">*</span></label>
                        <input id="first_name" type="text" className="form-control title_check  border-error" name="firstname" placeholder="Enter First Name" required />
                      </div>
                    </div>
                    <div className="col-lg-4 col-sm-6">
                      <div className="form-group">
                        <label className="label_txt">Middle Name</label>
                        <input id="middle_name" type="text" className="form-control" name="middlename" placeholder="Enter Middle Name"/>
                      </div>
                    </div>
                    <div className="col-lg-4 col-sm-6">
                      <div className="form-group">
                        <label className="label_txt required_fld">Last Name<span className="txt_red">*</span></label>
                        <input id="last_name" type="text" className="form-control title_check  border-error" name="lastname"  placeholder="Enter Last Name" required />
                      </div>
                    </div>
                    <div className="col-lg-4 col-sm-6">
                     <label className="label_txt required_fld">Date of Birth (DD/MM/YYYY)<span className="txt_red">*</span></label>
                     <div className="row margin_10">
                        <div className="col-xs-4">
                             <div className="form-group">
                                  <select id="birth_date" name="birth_date"  className="form-control  title_check  border-error" required >
                                <option value="">Select Date</option>
                                {optionsday.map(({ value, label }, index) => <option value={value} >{label}</option>)}
                            </select>
                      </div>
                        </div>
                        <div className="col-xs-4">
                            <div className="form-group">
                       <select id="birth_month" name="birth_month" className="form-control title_check  border-error" required>
                                <option value="">Select Month</option>
                                {optionsmonth.map(({ value, label }, index) => <option value={value} >{label}</option>)}
                            </select>
                      </div>
                        </div>
                        <div className="col-xs-4">
                             <div className="form-group">
                        <select id="birth_year" name="birth_year" className="form-control title_check  border-error" required>
                                <option value="">Select Year</option>
                                {optionsyear.map(({ value, label }, index) => <option value={value} >{label}</option>)}                
                            </select>
                      </div>
                        </div>
                     </div>

                    </div>
                    <div className="row_margin">
                      <div className="row margin_10">
                        <div className="col-lg-4 col-sm-6">
                          <div className="form-group">
                            <label className="label_txt required_fld">E-Mail Address<span className="txt_red">*</span></label>
                            <input id="email" type="email" className="form-control title_check  border-error" name="email" placeholder="Enter E-Mail Address" required />
                          </div>
                        </div>
                        <div className="col-lg-4 col-sm-6">
                          <div className="form-group">
                            <label className="label_txt required_fld">Confirm E-Mail Address<span className="txt_red">*</span></label>
                            <input id="email_confirm" type="email" className="form-control title_check  border-error" name="email_confirm" placeholder="Confirm E-Mail Address" required />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-4 col-sm-6">
                      <div className="form-group">
                        <label className="label_txt required_fld">Password<span className="txt_red">*</span></label>
                        <input id="password" type="password" className="form-control title_check  border-error" name="password" placeholder="Enter Password" required />
                      </div>
                    </div>
                    <div className="col-lg-4 col-sm-6">
                      <div className="form-group">
                        <label className="label_txt required_fld">Confirm Password<span className="txt_red">*</span></label>
                        <input id="password-confirm" type="password" className="form-control title_check  border-error" name="password_confirmation" placeholder="Confirm Password" required />
                      </div>
                    </div>
                    <div className="col-lg-4 col-sm-6">
                      <div className="form-group">
                        <label className="label_txt required_fld">Password Hint<span className="txt_red">*</span></label>
                        <input id="password_hint" type="text" className="form-control title_check  border-error" name="password_hint" placeholder="Password Hint" required />
                      </div>
                    </div>

                    <div className="col-sm-12">
                      <div className="row margin_10">
                        <div className="col-sm-6 col-md-4">
                          <div className="form-group">
                            <label className="label_txt required_fld">Mobile Phone Number<span className="txt_red">*</span></label>
                            <div className="row margin_10">
                              <div className="col-sm-4">
                                <div className="dropdown">
                                  <button type="button" className="form-control dropdown-toggle" onClick={ this.toggleMenu }>
                                      <img src={codeimgone}/>
                                      +{codeNameone}
                                  </button>                       
                                  <ul className={"dropdown-menu form_cont_menu " + show} onClick={ this.toggleMenu }>
                                    {countryData !==''?
                                      countryData.map(({ name, flagurl, flagname, phonecode}, index) => <li><a href="#" onChange={ this.getValue} ><a href="#"><img src={flagurl + flagname}/>{" +"+phonecode+" "+name}</a></a>
                                    </li>) : ""}
                                  </ul>
                                </div>
                              </div>
                              <div className="col-sm-8">
                                <input id="mobile_phone_number" type="number" className="form-control title_check  border-error" name="mobile_no" placeholder="Enter your phone" required />
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="col-sm-6 col-md-4">
                          <div className="form-group">
                            <label className="label_txt">Can we send you updates via WhatsApp?</label>
                            <div className="radio_boxes">
                              <span className="btn_box">
                                <input type="radio" id="what_appone" name="what_app" value="1"/>
                                <label for="what_appone"><span>Yes</span></label>
                              </span>
                              <span className="btn_box">
                                <input type="radio" id="what_app" name="what_app" value="0" checked="checked"/>
                                <label for="what_app"><span>No</span></label>
                              </span>
                            </div>
                          </div>
                        </div>
                        <div className="col-sm-6 col-md-4">
                          <div className="form-group">
                            <label className="label_txt">Can we send you updates via WeChat?</label>
                            <div className="radio_boxes">
                              <span className="btn_box">
                                <input type="radio" id="we_chata" ng-model="reg.user.we_chat" name="we_chat" value="1"/>
                                <label for="we_chata"><span>Yes</span></label>
                              </span>
                              <span className="btn_box">
                                <input type="radio" id="we_chat" ng-model="we_chat" name="we_chat" value="0" checked="checked" />
                                <label for="we_chat"><span>No</span></label>
                              </span>
                            </div>
                          </div>
                        </div>

           
                    </div>
                  </div>
                  <div className="loader text-center">
                  <span>
                          <div className="form-group">
                                  <ReCAPTCHA className="cap_block"
                                    sitekey="6LdKpzgUAAAAAINfiDD4ryBsLnTUD8_36k-m7J4t"
                                    onChange={onChange}
                                  />
                          </div>
                        </span>       
                    <div className="form-group">
                      <button type="submit" className="btn btn_dflt blue_btn blu_btn_shadow">Register</button>
                    </div>
                  </div>
                
              </div>
            </div>
            </form>
          </div>
          </div>
          </div>
          {/* Footer */}
          <FooterContainer />
          </div>

    )
  }
}





