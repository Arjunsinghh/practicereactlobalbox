import React, { Component } from 'react';
// import Perf from 'react-addons-perf';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux'
import { registerNav } from './modules/Navigation'
import { insertToken } from './redux/action/tokenAction'
import landingMarketplaceContainer from './pages/landingMarketplace/langingMarketplaceContainer'
import dealsandPromotionsContainer from './pages/dealsandPromotions/dealsandPromotionsContainer'
import landingMarketsegmentContainer from './pages/landingMarketsegment/landingMarketsegmentContainer'
import landingShopContainer from './pages/landingShop/landingShopContainer'
import loginContainer from './pages/login/loginContainer'
import signupContainer from './pages/signup/signupContainer'
import termsConditionContainer from './pages/termsCondition/termsConditionContainer'
import aboutUsContainer from './pages/aboutUs/aboutUsContainer'
import privacyPolicyContainer from './pages/privacyPolicy/privacyPolicyContainer'
import contactUsContainer from './pages/contactUs/contactUsContainer'
import landingProvinceContainer from './pages/landingProvince/landingProvinceContainer'
import marketplaceHomeContainer from './pages/marketplaceHome/marketplaceHomeContainer'
import HeaderContainer from './components/header/headerContainer'
import advanceSearchContainer from './pages/advanceSearch/advanceSearchContainer'
import homeContainer from './pages/home/homeContainer'


class App extends Component {
  componentDidMount() {
    this.props.insertToken()
  }
  render() {
    return (
      <span>
        <HeaderContainer />
        <Router ref={registerNav}>
          <Switch>
            <Route exact path="/landing-province" component={landingProvinceContainer} />
            <Route exact path="/landing-marketplace" component={landingMarketplaceContainer} />
            <Route exact path="/landing-deal-promotion" component={dealsandPromotionsContainer} />
            <Route exact path="/landing-market-segment" component={landingMarketsegmentContainer} />
            <Route exact path="/landing-shop" component={landingShopContainer} />
            <Route exact path="/login" component={loginContainer} />
            <Route exact path="/marketplace-home" component={marketplaceHomeContainer} />
            <Route exact path="/signup" component={signupContainer} />
            <Route exact path="/terms-condition" component={termsConditionContainer} />
            <Route exact path="/about-us" component={aboutUsContainer} />
            <Route exact path="/privacy-policy" component={privacyPolicyContainer} />
            <Route exact path="/contact-us" component={contactUsContainer} />
            <Route exact path="/advance-search" component={advanceSearchContainer} />
            <Route exact path="/home" component={homeContainer} />

            <Redirect to='/landing-marketplace' />
          </Switch>
        </Router>
        </span>
    );
  }
}
const mapStoreToProps = state => ({
  token: state.token.user_token
})
const mapDispatchToProps = {
  insertToken
}
export default connect(mapStoreToProps, mapDispatchToProps)(App);
