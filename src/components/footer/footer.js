import React, { Component } from 'react'
import './stylesheets/footer.css'
import jumpTo,{go} from '../../modules/Navigation'


export default class Footer extends Component {

  render() {

     const thisYear = 2021;

     const FedExlogo = require('../../assets/images/FedEx_logo.jpg');
     const Paytmforbusinessapp = require('../../assets/images/Paytm-for-business-app.jpg');
     const masterCard = require('../../assets/images/1529744317274.png');
     const VISALogo = require('../../assets/images/VISA_Logo.png');
     
    return (
        <span>
          <div className="below_hombann">
            <div className="container ">
                <div className="row">
                    <div className="col-xs-6 text-right first_slogn">
                        <div className="slogn_txt first_slogndata">
                            Our Platform - Your Business
                          </div>
                      </div>
                      <div className="col-xs-6 sec_slogn">
                        <div className="slogn_txt">
                            Your Success - Our Priority
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div className="footer_mainlink">
            <div className="container">
              <span href="#" title="BUY" className="main_fea bdr_nn">
                  BUY
              </span>
              <span href="#" className="main_fea" title="SELL">
                  SELL
              </span>
              <span href="#" className="main_fea" title="PROMOTE">
                  PROMOTE
              </span>
              <br/>
              <span href="#" className="main_fea" title="ANYTHING">
                  ANYTHING*
              </span>
              <span className="main_fea" href="#" title="ANYTIME ">
                  ANYTIME
              </span>
              <span href="#" title="ANYWHERE" className="main_fea">
                  ANYWHERE
              </span>
              <p className="foot_txt"> * <strong>GlobalBox</strong> will not permit or tolerate the commercialization of any illegal or inappropriate items </p>
            </div>
          </div>
          <footer className="footer">
            <div className="container ">
              <div className="row sm_space">
                <div className="col-sm-5 col-md-5">
                  <div className="foot_box linkbox"> <a  className="foot_link"> Home </a> <a onClick={() => jumpTo('/about-us')} className="foot_link"> About us </a> <a onClick={() => jumpTo('/terms-condition')} className="foot_link"> Terms Of Use </a> <a onClick={() => jumpTo('/contact-us')} className="foot_link"> Contact us</a><a onClick={() => jumpTo('/privacy-policy')} className="foot_link"> Privacy Policy </a></div>
                </div>
                <div className="col-sm-2 col-md-2 text-center social_con">
                  <div className="social_box"> <a href="#" title="twitter" className="soacil_link"> <i className="fab fa-twitter"></i></a> <a href="https://www.facebook.com/OfficialGlobalBox/"  className="soacil_link"title="facebook"> <i className="fab fa-facebook-f"></i> </a> <a href="https://www.linkedin.com/company/globalbox-world" title="linkedin" className="soacil_link"> <i className="fab fa-linkedin"></i> </a> <a href="https://www.instagram.com/globalbox_world/?hl=en" className="soacil_link" title="instagram"> <i className="fab fa-instagram"></i></a> 
                    <a href="https://www.youtube.com/channel/UC-jROZQGqqDkqltU-qHeLmw" className="soacil_link" title="youtube"> <i className="fab fa-youtube"></i> </a> </div>
                </div>
                <div className="col-sm-5 col-md-5 text-right">
                  <div className="foot_box copy_text"> © 2015-{thisYear} GlobalBox.world All rights reserved </div>
                </div>
              </div>
            </div>
          </footer>
          <footer className="footer_new">
            <div className="container ">
              <div className="row sm_space">
                <div className="col-sm-5 col-md-4">
                  <h4>Our Trusted Partners </h4>
                </div>
                <div className="col-sm-3 col-md-4 text-center social_con">
                  <div className="social_box"><a href="#" title="FedEx" className="soacil_link_new"><img src={FedExlogo} width="72" height="41" /></a><a href="#" title="PayTm" className="soacil_link_new"><img src={Paytmforbusinessapp} width="72" height="41" /></a><a href="#" title="Master Card" className="soacil_link_new"><img src={masterCard} width="72" height="41" /></a><a href="#" title="Visa" className="soacil_link_new"><img src={VISALogo} width="72" height="41" /></a></div>
                </div>
              </div>
            </div>
          </footer>

        </span>  
    )
  }
}



