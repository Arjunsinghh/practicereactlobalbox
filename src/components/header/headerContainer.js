import {connect} from 'react-redux'
import Header from './Header'
import {getViewSite} from '../../redux/action/headerAction'


const mapStoreToProps = state => ({
  languageName: state.header.headers,
})
const mapDispatchToProps = dispatch => ({
  getViewSite: ()=>dispatch(getViewSite()),
  // applyFilters:(filter_string)=>dispatch(applyFilters(filter_string))
})

export default connect(mapStoreToProps,mapDispatchToProps)(Header)