import React, { Component, useState } from 'react'
import './stylesheets/header.css'
import jumpTo,{go} from '../../modules/Navigation'
import Auth from '../../modules/Auth'

export default class Header extends Component {
  // const isLoggedin = 0;

  constructor(props) {
      super(props);
      this.state = {
        menu: false
      };
      this.props.getViewSite()
      this.toggleMenu = this.toggleMenu.bind(this);
    }

    toggleMenu(){
      this.setState({ menu: !this.state.menu })
    }

  render() {

    const isLoggedin = localStorage.getItem("isLoggedin") || 0;
    const userType = localStorage.getItem('userType');
    
    var countryLanguage = localStorage.getItem('countryLanguage');
    var languageName = localStorage.getItem('languageName');
    const conditionDataHeader = localStorage.getItem('conditionDataHeader');
    const wishlistCount = localStorage.getItem('wishlistCount');
    let companyName = localStorage.getItem('companyName');
    let userName = localStorage.getItem('userName');
    const show = (this.state.menu) ? "show" : "" ;
    const logo = require('../../assets/images/Arth.png');
     if(languageName === '' || languageName === null){
        if(this.props.languageName !== null && this.props.languageName !== undefined){
           countryLanguage = this.props.languageName.languagesCode;
           languageName = this.props.languageName.languagesName;
           localStorage.setItem('countryLanguage', countryLanguage );
           localStorage.setItem('languageName', languageName );
        }
    }

    if(userType === 'Vendor' && companyName !== '' && companyName !== null){
       userName =companyName;
    }else{
       userName =userName;
    }

    return (
      <div className="Top_Bar">
        <div className="container">
          <div className="row">
            <div className="col-sm-3 hidden-xs Top_column">
            {isLoggedin === 0 || isLoggedin === null?
              <div className="button_box">
                <a onClick={() =>  jumpTo('login')} className="btn login_btn coman_btn" ><i className="far fa-user"></i> Login</a>
                <div className="dropdown" >
                  <a className="btn blue_btn coman_btn dropdown-toggle" onClick={ this.toggleMenu }>
                          <i className="far fa-user"></i> JOIN US
                        </a>
                        <div align="right" className={"dropdown-menu Sub_menu " + show} onClick={ this.toggleMenu }>
                          <a onClick={() => jumpTo('/signup','Vendor')}>Register as Vendor </a>
                          <a onClick={() => jumpTo('/landing-shop')}>Register as Buyer </a>
                          <a  onClick={() => jumpTo('/landing-shop')}>Register as Affiliate </a>
                        </div>
                </div>
              </div> :
              <div className="button_box addbuttonclasshere"> 
                <div className="dropdown logged_btn">
                  <a href="#" className="dropdown-toggle">
                    <span className="circle_btn"><i className="far fa-user"></i></span><span className="user_txt">
                      {userName}
                    </span>
                  </a>
                  <div className="dropdown-menu Sub_menu" aria-labelledby="dropdownMenuButton">
                          <a href="/Dashboard"><i className="fas fa-tachometer-alt"></i>Dashboard</a>
                          <a href="/logout"><i className="fas fa-sign-out-alt"></i>Logout</a>
                      </div>
                </div>
              </div>
              }
            </div>
            <div className="col-sm-6 col-xs-7 text-center Top_column">          
                  <nav className="">
                      <div className="">
                        <div className="collapse navbar-collapse Example_Nav1" id="bs-example-navbar-collapse-1">
                          <div className="row">
                            <div className="col-lg-4 col-sm-4 col-md-4 col-lg-offset-1">
                              <a href="/landing-marketplace" className="media">
                                <div className="media-left">
                                  <span className="circle_btn" title="">
                                          <img src={logo} title="" alt="Arth"/>
                                       </span>
                                </div>
                                <div className="media-body">
                                        <h3 className="h3_active">GLOBAL MARKETPLACE</h3>
                                    </div>
                              </a>
                            </div>
                            <div className="col-lg-3 col-sm-4 col-md-4">
                              <a href="" className="media" title="Coming Soon">
                                <div className="media-left">
                                  <span className="circle_btn pink_circle" title="Coming Soon">
                                          <img src={logo} title="" alt="Arth"/>
                                       </span>
                                </div>
                                <div className="media-body">
                                        <h3 title="Coming Soon">GLOBAL SERVICES</h3>
                                    </div>
                              </a>
                            </div>
                            <div className="col-lg-4 col-sm-4 col-md-4 media_last">
                              <a href="" className="media" title="Coming Soon">
                                <div className="media-left" >
                                  <span className="circle_btn blue_circle" title="Coming Soon">
                                          <img src={logo} title="" alt="Arth"/>
                                       </span>
                                </div>
                                <div className="media-body">
                                        <h3 title="Coming Soon">GLOBAL PROMOTIONS</h3>
                                    </div>
                              </a>
                            </div>

                          </div>
                        </div>
                      </div>
                    </nav>
                </div>
                <div className="col-sm-3 col-xs-5 text-right Top_column">
                  <div className="head_icons">
                    <ul className="list-inline">
                      {userType === 'Buyer' ?
                      <li className="Heart_icon"> 
                        <a  title="click here for WishList">
                        <i className="far fa-heart"></i> 
                        <span className="noti_spn transition">{wishlistCount}</span> 
                        </a>
                      </li>: ""}
                      <li className="circle_box circle_Active">
                          <a href="#" className="circle_btn blue_circle  tooltip_box active">
                            en<span className="tooltiptext tooltiptext_box"><p>English</p>
                            </span>
                          </a>
                      </li>
                      { countryLanguage !== null ?
                      <li className="circle_box">
                          <a href="#" className="circle_btn blue_circle  tooltip_box">
                            {countryLanguage}<span className="tooltiptext tooltiptext_box"><p>{languageName} Coming Soon</p>
                            </span>
                          </a>          
                      </li>
                      : ""}
                    </ul>
                  </div>
                </div>
          </div>
        </div>
      </div>
    )
  }
}



