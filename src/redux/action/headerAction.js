import serverCall from '../../modules/serverCall'

export const getViewSite=()=>dispatch=>{
  dispatch({
    type:GET_HEADER_BEGIN,
  })
  return serverCall({
    method:'GET',
    url:`/api/header/site-view`
  })
  .then(res=>{
    dispatch({
      type: GET_HEADER_SUCCESS,
      payload: res
    })
    return res
  })
  .catch(error=>{
    dispatch({
      type: GET_HEADER_FAIL,
      payload: {error}
    })
    return error
  })
}

export const GET_HEADER_BEGIN = 'GET_HEADER_BEGIN'
export const GET_HEADER_SUCCESS='GET_HEADER_SUCCESS'
export const GET_HEADER_FAIL='GET_HEADER_FAIL'